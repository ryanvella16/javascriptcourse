  let firstName = 'Ryan';
  let lastName = 'Vella'; //string
  console.log(firstName,lastName);
  let interestRate = 0.3; //number
  const finalInterestRate = 1;
  console.log("Interest rate is " + interestRate);
  let isApproved = true; //boolean
  let selectedColor = null;
  let person = {
      name: 'Ryan',
      age: 22
  };
  person.name = 'John';
  person['name'] = 'Mary';
  console.log(person.name);

  let selectedColors = ['red', 'blue'];
  selectedColors[2] = 'green';
  console.log(selectedColors);

  function greet(name, lastName) {
      console.log('Hello ' + name + ' ' +lastName);
  }
  
  greet('John', 'Carabott');

  function square (number) {
      return number * number;
      
  }

  console.log(square(9));